<?php
    if (isset($_POST['people_id'])) {
        $people_id = $_POST["people_id"];
    
        include 'db_conn.php';
    
        try {
            $statement = $conn->prepare("DELETE FROM taitaja2018_osallistujat WHERE ID =:people_id");
            $statement->bindParam(":people_id",$people_id);
            $statement->execute();
        } catch (PDOException $ex) {
            echo $ex;
        }
        $conn = null;
    }
?>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Poista osallituja</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

    <link rel="stylesheet" type="text/css" media="screen" href="src/css/style.css"/>
    <script src="src/js/main.js"></script>
</head>
<body>
    <?php include 'base_nav.html';
    
    include 'db_conn.php';

    $event_id = $_GET['eid'];

    try {
        $query = $conn->prepare("SELECT * FROM taitaja2018 INNER JOIN taitaja2018_osallistujat
                                    ON taitaja2018.ID = taitaja2018_osallistujat.event_ID WHERE taitaja2018_osallistujat.event_ID = :event_id");
        $query->bindParam(":event_id",$event_id);
        $query->execute();
        $list_people = $query;
    } catch (PDOException $ex) {
        echo $ex;
    }

    ?>

    <section>
        <div class="container">
            <div class=section>
                <?php
                    try {
                        $query = $conn->prepare("SELECT * FROM taitaja2018 WHERE taitaja2018.ID = :event_id");
                        $query->bindParam(":event_id",$event_id);
                        $query->execute();

                        $result = $query->fetch();

                        $event_name = $result['event_name'];
                        $event_date = $result['event_date'];

                        echo "<div class=\"row center\"><h4>$event_name</h4></div>";
                        echo "<div class=\"row center\"><h5>$event_date</h5></div>";

                    } catch (PDOException $ex) {
                        echo $ex;
                    }                       

                    $conn = null;
                ?>
                    
            </div>
        </div>
    </section>


    <section>
        <div class="container">
            <div class="section">
                <div class="row center">
                    <h4>Poista osallituja</h4>
                </div>
                    <div class="row">
                        <ul id="list-people" class="collection with-header">
                            <li class="collection-header"><h4>Osallitujat</h4></li>
                            <?php
                            
                                foreach ($list_people as $row) {
                                    $event_name = $row["event_name"];
                                    $event_date = date('d.m.Y',strtotime($row["event_date"]));
                                    $name = $row['fullname'];
                                    $name_id = $row['ID'];

                                    echo "
                                    <li class=\"collection-item\">
                                        <div>
                                            $name".
                                            '<i people-id="'.$name_id.'" class="secondary-content event-delete-people material-icons">delete_forever</i>
                                        </div>
                                    </li>';
                                }
                                
                                $conn = null;
    
                            ?>
                        </ul>
                    </div>
            </div>
        </div>
    </section>

    <section id="promt-box" style="display: none">
        <div class="container">
            <div class="row">
                <div class="card-panel">
                    <form action="event_delete.php" method="post">
                        <div class="row">
                            <p>
                                Tässä tapahtumassa ei ole osallistujia
                            </p>
                        </div>
                        <div class="row">
                            <div class="input-field">
                                <a class="waves-effect waves-light btn" href="./manage.php">Takaisin</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <script>
        console.log(document.getElementById('list-people').children.length);
        if (document.getElementById('list-people').children.length == 1) {
            document.getElementById('promt-box').style.display = "block";
        }
    </script>
</body>
</html>