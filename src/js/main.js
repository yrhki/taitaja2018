var datepicker_options = {
    format:'dd.mm.yyyy',
    firstDay:1,
    i18n:{
        cancel:'Takaisin',
        done:'Valitse',
        months:[
            'Tammikuu',
            'Helmikuu',
            'Maaliskuu',
            'Huhtikuu',
            'Toukokuu',
            'Kesäkuu',
            'Heinäkuu',
            'Elokuu',
            'Syyskuu',
            'Lokakuu',
            'Marraskuu',
            'Joulukuu'
        ],
        monthsShort:[
            'Tammi',
            'Helmi',
            'Maalis',
            'Huhti',
            'Touko',
            'Kesä',
            'Heinä',
            'Elo',
            'Syys',
            'Loka',
            'Marras',
            'Joulu'
        ],
        weekdays:[
            'Maanantai',
            'Tiistai',
            'Keskiviikko',
            'Torstai',
            'Perjantai',
            'Lauantai',
            'Sunnuntai',
        ],
        weekdaysShort:[
            'Maa',
            'Tiistai',
            'Keskiviikko',
            'Torstai',
            'Perjantai',
            'Lauantai',
            'Sunnuntai',
        ],
        weekdaysAbbrev:[
            'SU',
            'MA',
            'TI',
            'KE',
            'TO',
            'PE',
            'LA'
        ],
    }
};

document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.datepicker');
    var instances = M.Datepicker.init(elems, datepicker_options);
});

$(document).ready(function(){
    $('.change-event').click(function(e){
        let elem = e.target.parentElement;
        let event_name = elem.parentElement.firstElementChild.textContent;
        let event_date = elem.parentElement.children[1].textContent;
        let event_id = elem.lastElementChild.textContent;

        let input_event_name = document.getElementById('event_name');
        let input_event_date = document.getElementById('event_date');
        let input_event_id = document.getElementById('event_id');

        input_event_name.value = event_name;
        input_event_date.value = event_date;
        input_event_id.value = event_id;

        document.getElementById('promt-box').style.display = "block";
        datepicker_options.defaultDate = new Date(event_date.split('.').reverse().join('-'));
        datepicker_options.setDefaultDate = true;
        let ins = M.Datepicker.init(document.querySelectorAll('.datepicker'),datepicker_options);
    });

    $('#promt-box-hide').click(function() {
        document.getElementById('promt-box').style.display = "none";
    })

    $('.delete-event').click(function(e) {
        let size = e.target.getAttribute('event-size');
        let id = e.target.getAttribute('event-id');
        
        if (Number(size) > 0) {
            document.getElementById('event_id').value = id;
            document.getElementById('promt-box').style.display = "block";
        } else {
            $.post('event_delete.php', {event_id:id}, function() {
                e.target.parentElement.parentElement.parentElement.removeChild(e.target.parentElement.parentElement);
            });
            
        }

    });

    $('.event-delete-people').click(function(e) {
        let pid = e.target.getAttribute('people-id');
        let amount = e.target.parentElement.parentElement.parentElement.children.length;
        
        $.post('event_less.php',{people_id:pid}, function() {
            e.target.parentElement.parentElement.parentElement.removeChild(e.target.parentElement.parentElement);
            amount--;

            if ((amount -1) == 0) {
                document.getElementById('promt-box').style.display = "block";
            }


        });
    })

});