<?php
if (isset($_POST['event_id'])) {
    $event_id = $_POST["event_id"];

    include 'db_conn.php';

    try {
        $statement = $conn->prepare("DELETE FROM taitaja2018 WHERE ID =:event_id");
        $statement->bindParam(":event_id",$event_id);
        $statement->execute();
            if (isset($_POST['event_action'])) {
                $statement = $conn->prepare("DELETE FROM taitaja2018_osallistujat WHERE event_ID =:event_id");
                $statement->bindParam(":event_id",$event_id);
                $statement->execute();
            }
    } catch (PDOException $ex) {
        echo $ex;
    }
    $conn = null;
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Poista tapahtumia</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

    <link rel="stylesheet" type="text/css" media="screen" href="src/css/style.css"/>
    <script src="src/js/main.js"></script>
</head>
<body>
    <?php include 'base_nav.html';?>
    <section>
        <div class="container">
            <div class="section">
                <div class="row center">
                    <h4>Poista tapahtuma</h4>
                </div>
                <form action="db_add.php" method="post">
                    <div class="row">
                        <table>
                            <tr>
                                <th>Tapahtuma</th>
                                <th>Päivämäärä</th>
                                <th>Toiminto</th>
                            </tr>
                            <?php
                            include 'db_conn.php';
                            $query = $conn->prepare("SELECT *,COUNT(taitaja2018_osallistujat.event_ID) FROM taitaja2018 LEFT JOIN taitaja2018_osallistujat ON taitaja2018.ID = taitaja2018_osallistujat.event_ID GROUP BY taitaja2018.ID");
                            $query->execute();

                            foreach ($query as $row) {
                                $event_name = $row["event_name"];
                                $event_id = $row[0];
                                $count = $row['COUNT(taitaja2018_osallistujat.event_ID)'];
                                $event_date = date('d.m.Y',strtotime($row["event_date"]));
                                
                                echo "<tr>
                                <td>$event_name</td>
                                <td>$event_date</td>
                                <td><a event-id=\"$event_id\" event-size=\"$count\" class=\"btn delete-event\">Poista</a></td>
                                
                                </tr>";
                            }
                            
                            $conn = null;
                            ?>


                            <tr>
                                
                            </tr>
                    </table>

                    </div>
                </form>
            </div>
        </div>
    </section>

    <section id="promt-box" style="display: none">
        <div class="container">
            <div class="row">
                <div class="card-panel">
                    <form action="event_delete.php" method="post">
                        <div class="row">
                            <p>
                                Tässä tapahtumassa on osallistujia
                            </p>
                        </div>
                        <div class="row">
                            <div class="input-field">
                                <button type="submit" class="waves-effect waves-light btn">Varmista</button>
                                <a class="waves-effect waves-light btn" id="promt-box-hide">Peruuta</a>
                            </div>
                            <div class="input-field hide">
                                <input type="text" name="event_action" id="event_size" value="remove_people">
                                <input id="event_id" placeholder="" name="event_id" type="text">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

</body>
</html>