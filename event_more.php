<?php
    if (isset($_POST['fullname'])) {
        $fullname = $_POST["fullname"];
        $event_id = $_GET['eid'];

        include 'db_conn.php';

        try {
            $statement = $conn->prepare("SELECT COUNT(taitaja2018_osallistujat.event_ID) as cnt FROM taitaja2018_osallistujat WHERE taitaja2018_osallistujat.event_ID = :event_ID");
            $statement->bindParam(":event_ID",$event_id);
            $statement->execute();

            $result = $statement->fetch();

            if ($result['cnt'] < 5) {
                $statement = $conn->prepare("INSERT INTO taitaja2018_osallistujat (fullname, event_ID) VALUES (:fullname,:event_ID)");
                $statement->bindParam(":fullname",$fullname);
                $statement->bindParam(":event_ID",$event_id);
                $statement->execute();
                $conn = null;
            }

        } catch (PDOException $ex) {
            echo $ex;
        }
    }
?>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Lisää osallituja</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

    <link rel="stylesheet" type="text/css" media="screen" href="src/css/style.css"/>
    <script src="src/js/main.js"></script>
</head>
<body>
    <?php include 'base_nav.html';
    
    include 'db_conn.php';

    $event_id = $_GET['eid'];

    try {
        $query = $conn->prepare("SELECT * FROM taitaja2018 INNER JOIN taitaja2018_osallistujat
                                    ON taitaja2018.ID = taitaja2018_osallistujat.event_ID WHERE taitaja2018_osallistujat.event_ID = :event_id");
        $query->bindParam(":event_id",$event_id);
        $query->execute();
        $list_people = $query;
        
    } catch (PDOException $ex) {
        echo $ex;
    }
    ?>

    <section>
        <div class="container">
            <div class=section>
                <?php
                    try {
                        $query = $conn->prepare("SELECT * FROM taitaja2018 WHERE taitaja2018.ID = :event_id");
                        $query->bindParam(":event_id",$event_id);
                        $query->execute();

                        $result = $query->fetch();

                        $event_name = $result['event_name'];
                        $event_date = $result['event_date'];

                        echo "<div class=\"row center\"><h4>$event_name</h4></div>";
                        echo "<div class=\"row center\"><h5>$event_date</h5></div>";

                    } catch (PDOException $ex) {
                        echo $ex;
                    }                       

                    $conn = null;
                ?>
                    
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="section">
                <div class="row center">
                    <h4>Lisää osallituja</h4>
                </div>
                    <div class="row">
                        <ul class="collection with-header">
                            <li class="collection-header"><h4>Osallitujat</h4></li>
                            <?php
                            $count = 0;

                            foreach ($list_people as $row) {
                                $count++;
                                $event_name = $row["event_name"];
                                $event_date = date('d.m.Y',strtotime($row["event_date"]));
                                $name = $row['fullname'];
                    
                                echo "<li class=\"collection-item\">$name</li>";
                            }

                            if ($count < 5) {
                                echo '
                                <li class="collections-item">
                                    <form action="event_more.php?eid='.$event_id.'" method="post">
                                        <div class="input-field col s6">
                                            <input id="fullname" placeholder="" name="fullname" type="text">
                                            <label for="fullname">Nimi</label>
                                        </div>
                                        <div class="input-field col s6">
                                            <button type="submit" class="secondary-content waves-effect waves-light btn">Lisää</button>
                                        </div>
                                    </form>
                                </li>';
                            }
                            
                            ?>
                            
                        </ul>
                    </div>
            </div>
        </div>
    </section>
</body>
</html>