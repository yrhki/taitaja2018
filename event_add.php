<?php
    if (isset($_POST['title'])) {
        $event_name = $_POST["title"];
        $event_date = $_POST["date"];
        $event_date = date("Y-m-d",strtotime($event_date));
        include 'db_conn.php';
    
        try {
            $statement = $conn->prepare("INSERT INTO taitaja2018 (event_name, event_date) VALUES (:event_name,:event_date)");
            $statement->bindParam(":event_name",$event_name);
            $statement->bindParam(":event_date",$event_date);
            
            $statement->execute();
            $conn = null;
        } catch (PDOException $ex) {
            echo $ex;
        }
    }
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Lisää tapahtumia</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

    <link rel="stylesheet" type="text/css" media="screen" href="src/css/style.css"/>
    <script src="src/js/main.js"></script>
</head>
<body>
    <?php include 'base_nav.html';?>

    <section>
        <div class="container">
            <div class="section">
                <div class="row center">
                    <h4>Lisää tapahtuma</h4>
                </div>
                <form action="event_add.php" method="post">
                    <div class="row">
                        <div class="input-field col s6">
                            <input id="event_name" name=title type="text">
                            <label for="event_name">Tapahtuman nimi</label>
                        </div>
                        <div class="input-field col s6">
                            <input id="event_date" name="date" type="text" class="datepicker">
                            <label for="event_date">Tapahtuman päivämäärä</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field">
                            <button type="submit" class="waves-effect waves-light btn">Lähetä</button>
                            <a href="./" class="waves-effect waves-light btn">Takaisin</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>





</body>
</html>