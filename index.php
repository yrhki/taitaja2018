<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Etusivu</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

    <link rel="stylesheet" type="text/css" media="screen" href="src/css/style.css"/>
    <script src="src/js/main.js"></script>
</head>
<body>
    <?php include 'base_nav.html';?>

    

</body>
</html>