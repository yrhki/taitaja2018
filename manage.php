<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Hallitse tapahtumia</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

    <link rel="stylesheet" type="text/css" media="screen" href="src/css/style.css"/>
    <script src="src/js/main.js"></script>
</head>
<body>
    <?php include 'base_nav.html';?>
    <section>
        <div class="container">
            <div class="section">
                <div class="row center">
                    <h4>Hallitse tapahtuma</h4>
                </div>
                <form action="db_add.php" method="post">
                    <div class="row">
                        <table>
                            <tr>
                                <th>Tapahtuma</th>
                                <th>Päivämäärä</th>
                                <th>Toiminto</th>
                            </tr>
                            <?php
                            include 'db_conn.php';
                            $query = $conn->prepare("SELECT * FROM taitaja2018");
                            $query->execute();

                            foreach ($query as $row) {
                                $event_name = $row["event_name"];
                                $event_date = date('d.m.Y',strtotime($row["event_date"]));
                                $event_id = $row['ID'];

                                echo "<tr>
                                <td>$event_name</td>
                                <td>$event_date</td>
                                <td><a href=\"event_more.php?eid=$event_id\" class=\"btn\">Lisää osallistuja</a></td>
                                <td><a href=\"event_less.php?eid=$event_id\" class=\"btn\">Poista osallistuja</a></td>
                                </tr>";
                            }
                            $conn = null;
                            ?>
                    </table>
                    </div>
                </form>
            </div>
        </div>
    </section>
</body>
</html>