-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Generation Time: Jan 28, 2019 at 08:56 AM
-- Server version: 10.1.26-MariaDB-0+deb9u1
-- PHP Version: 7.0.33-0+deb9u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

-- Database: `web_projects`
--
CREATE DATABASE IF NOT EXISTS `web_projects` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `web_projects`;

-- --------------------------------------------------------

--
-- Table structure for table `taitaja2018`
--

CREATE TABLE `taitaja2018` (
  `ID` int(10) UNSIGNED NOT NULL,
  `event_name` varchar(30) NOT NULL,
  `event_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `taitaja2018`
--

INSERT INTO `taitaja2018` (`ID`, `event_name`, `event_date`) VALUES
(24, 'asdASDadadsd', '2019-03-31');

-- --------------------------------------------------------

--
-- Table structure for table `taitaja2018_osallistujat`
--

CREATE TABLE `taitaja2018_osallistujat` (
  `ID` int(10) UNSIGNED NOT NULL,
  `fullname` varchar(60) NOT NULL,
  `event_ID` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `taitaja2018_osallistujat`
--

INSERT INTO `taitaja2018_osallistujat` (`ID`, `fullname`, `event_ID`) VALUES
(34, 'hhtddjtjtjtdjt', 24);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `taitaja2018`
--
ALTER TABLE `taitaja2018`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `taitaja2018_osallistujat`
--
ALTER TABLE `taitaja2018_osallistujat`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `taitaja2018`
--
ALTER TABLE `taitaja2018`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `taitaja2018_osallistujat`
--
ALTER TABLE `taitaja2018_osallistujat`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

